package hernan.cristobal.clinicajuridica;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MenuPrincipal extends AppCompatActivity {
    String rut = "";
    String nombre_cliente = "";
    String nombre_alumno = "";
    String titulo = "";
    String descripcion = "";
    String tipo = "";
    String text ="";

    JSONArray jArray;
    Context ctx;
    LinearLayout linearLayout_asignaciones;
    static SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = getApplicationContext();
        setContentView(R.layout.activity_menu_principal);
        rut = getIntent().getStringExtra("rut");
        linearLayout_asignaciones = (LinearLayout) findViewById(R.id.linearlayout_asignaciones);
        jArray = new JSONArray();
        new CargarHorario().execute();
    }

    private class CargarHorario extends AsyncTask<Void, Void, Integer> {
        private ProgressDialog Dialog = new ProgressDialog(MenuPrincipal.this);

        @Override
        protected void onPreExecute() {
            Dialog.setMessage("Cargando Horario...");
            Dialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            String urlString = ClinicaJuridicaConf.WEBSERVICE_BASE_URL + "index.php/horario/traer_horario_json/?rut="+rut;
            StringBuffer chaine = new StringBuffer("");
            try {
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("User-Agent", "");
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.connect();
                StringBuilder sb = new StringBuilder();
                InputStream inputStream = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                }
                JSONObject json = new JSONObject(sb.toString());
                jArray = json.getJSONArray("horario");


            } catch (IOException e) {
                // writing exception to log
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {

            if (result == 0) {
                int int_color = 0;
                boolean lunes = true;
                boolean martes = true;
                boolean miercoles = true;
                boolean jueves = true;
                boolean viernes = true;



                for (int i = 0; i < jArray.length(); i++) {
                    try {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        if ("Lunes".equals(oneObject.getString("dia"))) {
                            if (lunes) {
                                TextView nueva_asignacion = new TextView(ctx);
                                ImageView separador = new ImageView(ctx);
                                separador.setImageResource(R.drawable.barrablanca);
                                nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                                nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                               nueva_asignacion.setHeight(90);
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                                nueva_asignacion.setText("Lunes " + oneObject.getString("fecha_asignacion"));
                                linearLayout_asignaciones.addView(nueva_asignacion);

                                lunes = false;
                            }

                            String dia = oneObject.getString("dia");
                            //String id_causa = oneObject.getString("id_causa");
                            tipo = oneObject.getString("tipo");
                            String id_asignacion = oneObject.getString("id_asignacion");
                            String fecha_asignacion = oneObject.getString("fecha_asignacion");
                            String hora_inicio = oneObject.getString("hora_inicio");
                            String hora_fin = oneObject.getString("hora_fin");
                            if(tipo.equalsIgnoreCase("Causa")){
                                nombre_alumno = oneObject.getString("nombre_alumno");
                                nombre_cliente = oneObject.getString("nombre_cliente");
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                titulo = oneObject.getString("titulo");
                                descripcion = oneObject.getString("descripcion");
                            }
                            TextView nueva_asignacion = new TextView(ctx);
                            ImageView separador = new ImageView(ctx);
                            separador.setImageResource(R.drawable.barrablanca);
                            nueva_asignacion.setTextColor(getResources().getColor(R.color.gris_claro));
                            nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                            if (int_color == 0) {
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.blanco));
                                int_color = 1;
                            } else {
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.gris_perfil));
                                int_color = 0;
                            }
                            nueva_asignacion.setText(hora_inicio + " / " + hora_fin + " Tipo: " + tipo);
                           nueva_asignacion.setHeight(70);
                            linearLayout_asignaciones.addView(nueva_asignacion);
                            final int finalI = i;
                            if(tipo.equalsIgnoreCase("Causa")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_causa(finalI);
                                    }
                                });
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_asunto(finalI);
                                    }
                                });
                            }

                        }
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }

                for (int i = 0; i < jArray.length(); i++) {
                    try {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        if("Martes".equals(oneObject.getString("dia"))){
                            if(martes){
                                TextView nueva_asignacion = new TextView(ctx);
                                ImageView separador = new ImageView(ctx);
                                separador.setImageResource(R.drawable.barrablanca);
                                nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                                nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                               nueva_asignacion.setHeight(90);
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                                nueva_asignacion.setText("Martes "+oneObject.getString("fecha_asignacion"));
                                linearLayout_asignaciones.addView(nueva_asignacion);
                                martes = false;
                            }

                        String dia = oneObject.getString("dia");
                        //String id_causa = oneObject.getString("id_causa");
                        tipo = oneObject.getString("tipo");
                        String id_asignacion = oneObject.getString("id_asignacion");
                        String fecha_asignacion = oneObject.getString("fecha_asignacion");
                        String hora_inicio = oneObject.getString("hora_inicio");
                        String hora_fin = oneObject.getString("hora_fin");
                            if(tipo.equalsIgnoreCase("Causa")){
                                nombre_alumno = oneObject.getString("nombre_alumno");
                                nombre_cliente = oneObject.getString("nombre_cliente");
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                titulo = oneObject.getString("titulo");
                                descripcion = oneObject.getString("descripcion");
                            }
                        TextView nueva_asignacion = new TextView(ctx);
                        ImageView separador = new ImageView(ctx);
                        separador.setImageResource(R.drawable.barrablanca);
                        nueva_asignacion.setTextColor(getResources().getColor(R.color.gris_claro));
                        nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                        if (int_color == 0) {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.blanco));
                            int_color = 1;
                        } else {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.gris_perfil));
                            int_color = 0;
                        }
                        nueva_asignacion.setText(hora_inicio + " / " + hora_fin+ " Tipo: " + tipo);
                            nueva_asignacion.setHeight(70);
                        linearLayout_asignaciones.addView(nueva_asignacion);
                            final int finalI = i;
                            if(tipo.equalsIgnoreCase("Causa")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_causa(finalI);
                                    }
                                });
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_asunto(finalI);
                                    }
                                });
                            }

                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }

            }

                for (int i = 0; i < jArray.length(); i++) {
                    try {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        if("Miercoles".equals(oneObject.getString("dia"))){
                            if(miercoles){
                                TextView nueva_asignacion = new TextView(ctx);
                                ImageView separador = new ImageView(ctx);
                                separador.setImageResource(R.drawable.barrablanca);
                                nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                                nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                               nueva_asignacion.setHeight(90);
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                                nueva_asignacion.setText("Miercoles "+oneObject.getString("fecha_asignacion"));
                                linearLayout_asignaciones.addView(nueva_asignacion);
                                miercoles = false;
                            }

                        String dia = oneObject.getString("dia");
                        //String id_causa = oneObject.getString("id_causa");
                        tipo = oneObject.getString("tipo");
                        String id_asignacion = oneObject.getString("id_asignacion");
                        String fecha_asignacion = oneObject.getString("fecha_asignacion");
                        String hora_inicio = oneObject.getString("hora_inicio");
                        String hora_fin = oneObject.getString("hora_fin");
                            if(tipo.equalsIgnoreCase("Causa")){
                                nombre_alumno = oneObject.getString("nombre_alumno");
                                nombre_cliente = oneObject.getString("nombre_cliente");
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                titulo = oneObject.getString("titulo");
                                descripcion = oneObject.getString("descripcion");
                            }
                        TextView nueva_asignacion = new TextView(ctx);
                        ImageView separador = new ImageView(ctx);
                        separador.setImageResource(R.drawable.barrablanca);
                        nueva_asignacion.setTextColor(getResources().getColor(R.color.gris_claro));
                        nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                        if (int_color == 0) {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.blanco));
                            int_color = 1;
                        } else {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.gris_perfil));
                            int_color = 0;
                        }
                        nueva_asignacion.setText(hora_inicio + " / " + hora_fin+ " Tipo: " + tipo);
                            nueva_asignacion.setHeight(70);
                        linearLayout_asignaciones.addView(nueva_asignacion);
                            final int finalI = i;
                            if(tipo.equalsIgnoreCase("Causa")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                            abrir_detalle_causa(finalI);
                                    }
                                });
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_asunto(finalI);
                                    }
                                });
                            }


                        }
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }

                for (int i = 0; i < jArray.length(); i++) {
                    try {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        if("Jueves".equals(oneObject.getString("dia"))){
                            if(jueves){
                                TextView nueva_asignacion = new TextView(ctx);
                                ImageView separador = new ImageView(ctx);
                                separador.setImageResource(R.drawable.barrablanca);
                                nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                                nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                               nueva_asignacion.setHeight(90);
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                                nueva_asignacion.setText("Jueves "+oneObject.getString("fecha_asignacion"));
                                linearLayout_asignaciones.addView(nueva_asignacion);
                                jueves = false;
                            }

                        String dia = oneObject.getString("dia");
                        //String id_causa = oneObject.getString("id_causa");
                        tipo = oneObject.getString("tipo");
                        String id_asignacion = oneObject.getString("id_asignacion");
                        String fecha_asignacion = oneObject.getString("fecha_asignacion");
                        String hora_inicio = oneObject.getString("hora_inicio");
                        String hora_fin = oneObject.getString("hora_fin");
                            if(tipo.equalsIgnoreCase("Causa")){
                                nombre_alumno = oneObject.getString("nombre_alumno");
                                nombre_cliente = oneObject.getString("nombre_cliente");
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                titulo = oneObject.getString("titulo");
                                descripcion = oneObject.getString("descripcion");
                            }
                        TextView nueva_asignacion = new TextView(ctx);
                        ImageView separador = new ImageView(ctx);
                        separador.setImageResource(R.drawable.barrablanca);
                        nueva_asignacion.setTextColor(getResources().getColor(R.color.gris_claro));
                        nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                        if (int_color == 0) {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.blanco));
                            int_color = 1;
                        } else {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.gris_perfil));
                            int_color = 0;
                        }
                        nueva_asignacion.setText(hora_inicio + " / " + hora_fin+ " Tipo: " + tipo);
                            nueva_asignacion.setHeight(70);
                        linearLayout_asignaciones.addView(nueva_asignacion);
                            final int finalI = i;
                            if(tipo.equalsIgnoreCase("Causa")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_causa(finalI);
                                    }
                                });
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_asunto(finalI);
                                    }
                                });
                            }
                        }
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }

                for (int i = 0; i < jArray.length(); i++) {
                    try {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        if("Viernes".equals(oneObject.getString("dia"))){
                            if(viernes){
                                TextView nueva_asignacion = new TextView(ctx);
                                ImageView separador = new ImageView(ctx);
                                separador.setImageResource(R.drawable.barrablanca);
                                nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                                nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                               nueva_asignacion.setHeight(90);
                                nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                                nueva_asignacion.setText("Viernes "+oneObject.getString("fecha_asignacion"));
                                linearLayout_asignaciones.addView(nueva_asignacion);
                                viernes = false;
                            }

                        String dia = oneObject.getString("dia");
                        //String id_causa = oneObject.getString("id_causa");
                        tipo = oneObject.getString("tipo");
                        String id_asignacion = oneObject.getString("id_asignacion");
                        String fecha_asignacion = oneObject.getString("fecha_asignacion");
                        String hora_inicio = oneObject.getString("hora_inicio");
                        String hora_fin = oneObject.getString("hora_fin");
                            if(tipo.equalsIgnoreCase("Causa")){
                                nombre_alumno = oneObject.getString("nombre_alumno");
                                nombre_cliente = oneObject.getString("nombre_cliente");
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                titulo = oneObject.getString("titulo");
                                descripcion = oneObject.getString("descripcion");
                            }
                        TextView nueva_asignacion = new TextView(ctx);
                        ImageView separador = new ImageView(ctx);
                        separador.setImageResource(R.drawable.barrablanca);
                        nueva_asignacion.setTextColor(getResources().getColor(R.color.gris_claro));
                        nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                        if (int_color == 0) {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.blanco));
                            int_color = 1;
                        } else {
                            nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.gris_perfil));
                            int_color = 0;
                        }
                        nueva_asignacion.setText(hora_inicio + " / " + hora_fin+ " Tipo: " + tipo);
                            nueva_asignacion.setHeight(70);
                        linearLayout_asignaciones.addView(nueva_asignacion);
                            final int finalI = i;
                            if(tipo.equalsIgnoreCase("Causa")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_causa(finalI);
                                    }
                                });
                            }else if(tipo.equalsIgnoreCase("Asunto")){
                                nueva_asignacion.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        abrir_detalle_asunto(finalI);
                                    }
                                });
                            }
                        }
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }

                if(lunes && martes && miercoles && jueves && viernes){
                    TextView nueva_asignacion = new TextView(ctx);
                    ImageView separador = new ImageView(ctx);
                    separador.setImageResource(R.drawable.barrablanca);
                    nueva_asignacion.setTextColor(getResources().getColor(R.color.blanco));
                    nueva_asignacion.setTextSize(getResources().getDimension(R.dimen.dpde10));
                    nueva_asignacion.setBackgroundColor(getResources().getColor(R.color.cafeoscuro));
                    nueva_asignacion.setText("Sin Causas/Asuntos Asignados");
                    linearLayout_asignaciones.addView(nueva_asignacion);
                }


                Dialog.dismiss();
            }
        }
    }



    public void CerrarSesion( View view){
        DbUser db_user =  new DbUser(ctx, "DBUSER", null, 1);
        db = db_user.getWritableDatabase();
        db.execSQL("DELETE FROM Usuario;");
        db.close();
        Intent intent = new Intent(ctx, LoginActivity.class);
        startActivityForResult(intent, 0);

    }
    public void Actualizar( View view){
        linearLayout_asignaciones.removeAllViews();
        new CargarHorario().execute();

    }

    public void abrir_detalle_causa(int id_objeto) {
        try {
            String p_nombre_alumno = jArray.getJSONObject(id_objeto).getString("nombre_alumno");
            String p_fecha_asignacion = jArray.getJSONObject(id_objeto).getString("fecha_asignacion");
            String p_hora_inicio = jArray.getJSONObject(id_objeto).getString("hora_inicio");
            String p_hora_fin = jArray.getJSONObject(id_objeto).getString("hora_fin");
            String p_id_asignacion = jArray.getJSONObject(id_objeto).getString("id_asignacion");
            String p_nombre_cliente = jArray.getJSONObject(id_objeto).getString("nombre_cliente");

            Intent intent = new Intent(ctx, DetalleCausa.class);
            intent.putExtra("nombre_alumno", p_nombre_alumno);
            intent.putExtra("fecha_asignacion", p_fecha_asignacion);
            intent.putExtra("hora_inicio", p_hora_inicio);
            intent.putExtra("hora_fin", p_hora_fin);
            intent.putExtra("id_asignacion", p_id_asignacion);
            intent.putExtra("nombre_cliente", p_nombre_cliente);
            startActivityForResult(intent, 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    public void abrir_detalle_asunto(int id_objeto) {
        try {

            String p_fecha_asignacion = jArray.getJSONObject(id_objeto).getString("fecha_asignacion");
            String p_hora_inicio = jArray.getJSONObject(id_objeto).getString("hora_inicio");
            String p_hora_fin = jArray.getJSONObject(id_objeto).getString("hora_fin");
            String p_id_asignacion = jArray.getJSONObject(id_objeto).getString("id_asignacion");
            String p_titulo = jArray.getJSONObject(id_objeto).getString("titulo");
            String p_detalle = jArray.getJSONObject(id_objeto).getString("descripcion");

            Intent intent = new Intent(ctx, DetalleAsunto.class);
            intent.putExtra("titulo", p_titulo);
            intent.putExtra("fecha_asignacion", p_fecha_asignacion);
            intent.putExtra("hora_inicio", p_hora_inicio);
            intent.putExtra("hora_fin", p_hora_fin);
            intent.putExtra("id_asignacion", p_id_asignacion);
            intent.putExtra("descripcion", p_detalle);
            startActivityForResult(intent, 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





}

