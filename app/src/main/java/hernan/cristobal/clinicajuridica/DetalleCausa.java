package hernan.cristobal.clinicajuridica;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class DetalleCausa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_asignacion);

        String nombre_alumno = getIntent().getStringExtra("nombre_alumno");
        String fecha_asignacion = getIntent().getStringExtra("fecha_asignacion");
        String hora_inicio = getIntent().getStringExtra("hora_inicio");
        String hora_fin = getIntent().getStringExtra("hora_fin");
        String id_asignacion = getIntent().getStringExtra("id_asignacion");
        String nombre_cliente = getIntent().getStringExtra("nombre_cliente");
        String tipo = getIntent().getStringExtra("tipo");

        TextView nombre_alumno_v	=	(TextView) findViewById(R.id.alumnov);
        nombre_alumno_v.setText("Nombre Alumno: "+nombre_alumno);
        TextView  fecha_asignacion_v	=	(TextView) findViewById(R.id.fechav);
        fecha_asignacion_v.setText("Fecha: "+fecha_asignacion);
        TextView  hora_inicio_v	=	(TextView) findViewById(R.id.iniciov);
        hora_inicio_v.setText("Hora Inicio: "+hora_inicio);
        TextView  hora_fin_v	=	(TextView) findViewById(R.id.terminov);
        hora_fin_v.setText("Hora Fin: "+hora_fin);
        TextView  nombre_cliente_v	=	(TextView) findViewById(R.id.usuariov);
        nombre_cliente_v.setText("Nombre Usuario: "+nombre_cliente);










    }



}
